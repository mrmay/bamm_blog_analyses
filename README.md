## Constant-rate analyses
The analyses/constant\_rates directory contains BAMM config files in two subdirectories, as well as a data subdirectory (containing the constant-rate phylogenies simulated for our PNAS paper). The BAMM\_without\_bug subdirectory contains scripts for analyses with combineExtinctionAtNodes = random; BAMM\_with\_bug contains scripts for analyses with combineExtinctionAtNodes = if\_different. The remaining directories contain scripts for analyses with the named combineExtinctionAtNodes options.

These config files can be executed by navigating to the analyses/constant\_rates directory and pointing BAMM 2.5 to the config file in the desired subdirectory (e.g. bamm -c BAMM\_with\_bug/tree\_1/gamma\_0.1/control.txt).

## Empirical analyses
The analyses/cetacean\_tree directory contains BAMM config files in two subdirectories, as well as a data subdirectory (containing the cetacean phylogeny from Steeman 2009 Syst. Biol.). The structure of this directory is the same as the constant-rate analysis, but only contains scripts for the combineExtinctionAtNodes = if_different and random options.

## Variable-rate analyses
The analyses/constant\_rates directory contains BAMM config files in two subdirectories, as well as a data subdirectory (containing the constant-rate phylogenies simulated for our PNAS paper). The structure of this directory is the same as the constant-rate analysis, but only contains scripts for the combineExtinctionAtNodes = if_different and random options.

In addition to .tre files, the data directory contains two .csv files, rate_estimates.csv and rate_estimates_if_different.csv. Each of these files contains the true branch-specific diversification-rate estimates for each tree when analyzed with gamma = 1 (one row per branch). Additionally, this directory contains .Rda files for each simulated tree which contain the complete information about the number, location, and magnitude of diversification-rate shifts (these can be read into R using load("file name")).

## Simmer
This directory contains a program, simmer, that simulates and displays outcomes under the birth-death with rate shifts. 
This program can be used to test your intution about the meaning of the extinciton probability. (It's simply the probability that the process dies before the present.) This program allows rates to shift under a model similar to that described by Rabosky (2014). (Note that this program will run under Mac OSX.)
