library(BAMMtools)
setwd("~/repos/bamm_web_campaign/analyses/variable_rates/")

# Read the template
template <- readLines("~/repos/bamm_web_campaign/analyses/variable_rates/src/template.txt")
template[246] <- "combineExtinctionAtNodes = if_different"

# Settings for this simulation
trees <- 1:100
gamma <- c(0.1,0.5,1,2,10)

# Make configs files for each of the 100 trees

# loop over trees
for(t in trees) {

  # read the tree
  this_tree_dir <- paste0("~/repos/bamm_web_campaign/analyses/variable_rates/data/sim_",t,".tre")
  this_tree     <- read.tree(this_tree_dir)

  # get priors for this tree
  priors       <- setBAMMpriors(this_tree,outfile=NULL)
  lambda_init  <- priors[2,2]
  lambda_shift <- priors[3,2]
  mu_init      <- priors[4,2]

  # copy the template and insert the relevant information
  control_file     <- template
  control_file[19] <- paste0("treefile = data/sim_",t,".tre")
  control_file[79] <- paste0("lambdaInitPrior = ",lambda_init)
  control_file[83] <- paste0("lambdaShiftPrior = ",lambda_shift)
  control_file[88] <- paste0("muInitPrior = ",mu_init)

  # loop over prior settings for expected number of events
  for(g in gamma) {

    # get the directory for this prior
    this_directory    <- paste0("BAMM_with_bug/tree_",t,"/gamma_",g)
    dir.create(this_directory,recursive = TRUE,showWarnings = FALSE)

    # make the control file for this prior
    this_control_file      <- control_file
    this_control_file[22]  <- paste0("runInfoFilename = ",this_directory,"/runinfo.txt")
    this_control_file[71]  <- paste0("poissonRatePrior = ",g)
    this_control_file[102] <- paste0("mcmcOutfile = ",this_directory,"/mcmc_out.txt")
    this_control_file[109] <- paste0("eventDataOutfile = ",this_directory,"/event_out.txt")

    # write the control file
    writeLines(this_control_file,paste0(this_directory,"/control.txt"))

  }

}















