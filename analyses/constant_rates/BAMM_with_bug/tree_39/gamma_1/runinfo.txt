Command line: ./bamm -c BAMM_with_bug/tree_39/gamma_1/control.txt
Git commit id: 
Random seed: 1467701086
Start time: Mon Jul  4 23:44:46 2016

Current parameter settings:
       acceptanceInfoFileName    acceptance_info.txt
          acceptanceResetFreq    1000
            alwaysRecomputeE0    0
                     autotune    0
         branchRatesWriteFreq    0
            chainSwapFileName    chain_swap.txt
             checkUltrametric    1
     combineExtinctionAtNodes    if_different
          conditionOnSurvival    -1
                       deltaT    0.01
              eventDataInfile    event_data_in.txt
             eventDataOutfile    BAMM_with_bug/tree_39/gamma_1/event_out.txt
           eventDataWriteFreq    1000
       expectedNumberOfShifts    0.0
            extinctionProbMax    0.9999
fastSimulatePriorExperimental    0
     fastSimulatePrior_BurnIn    0.05
fastSimulatePrior_Generations    5000000
 fastSimulatePrior_SampleFreq    50
       globalSamplingFraction    1.0
          initialNumberEvents    0
              initializeModel    1
                  lambdaInit0    0.032
              lambdaInitPrior    10.5966440428316
          lambdaInitRootPrior    -1.0
    lambdaIsTimeVariablePrior    1
                lambdaOutfile    lambda_rates.txt
                 lambdaShift0    0
             lambdaShiftPrior    0.05
         lambdaShiftRootPrior    -1.0
                loadEventData    0
         localGlobalMoveRatio    10.0
              maxNumberEvents    5000
                  mcmcOutfile    BAMM_with_bug/tree_39/gamma_1/mcmc_out.txt
                mcmcWriteFreq    1000
         minCladeSizeForShift    1
                    modeltype    speciationextinction
                      muInit0    0.005
                  muInitPrior    83.4264929170908
              muInitRootPrior    -1.0
                    muOutfile    mu_rates.txt
                     muShift0    0.0
                 muShiftPrior    1.0
             muShiftRootPrior    -1.0
            numberOccurrences    0
               numberOfChains    1
          numberOfGenerations    10000000
              observationTime    -1
                      outName    
         outputAcceptanceInfo    0
                    overwrite    0
             poissonRatePrior    1
         preservationRateInit    0
        preservationRatePrior    1.0
                    printFreq    1000
          priorOutputFileName    prior_probs.txt
 priorSim_IntervalGenerations    5000
              runInfoFilename    BAMM_with_bug/tree_39/gamma_1/runinfo.txt
                      runMCMC    1
          sampleFromPriorOnly    0
          sampleProbsFilename    sample_probs.txt
                         seed    -1
                    segLength    0.02
          simulatePriorShifts    0
                   swapPeriod    1000
                     treefile    data/sim_39.tre
     updateEventLocationScale    0.05
         updateEventRateScale    4.0
        updateLambdaInitScale    2.0
       updateLambdaShiftScale    0.1
            updateMuInitScale    2.0
           updateMuShiftScale    0.0
  updatePreservationRateScale    1.0
        updateRateEventNumber    0.1
updateRateEventNumberForBranch    0.0
      updateRateEventPosition    1
          updateRateEventRate    1
            updateRateLambda0    1
        updateRateLambdaShift    1
     updateRateLambdaTimeMode    0
                updateRateMu0    1
            updateRateMuShift    0.0
   updateRatePreservationRate    -1
 useGlobalSamplingProbability    1
   validateEventConfiguration    0
   writeMeanBranchLengthTrees    0
End time: Tue Jul  5 00:06:49 2016

